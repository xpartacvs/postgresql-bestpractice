#!/bin/bash

function help {
  echo "Usage: $0 [OPTIONS]"
  echo ""
  echo "OPTIONS:"
  echo "  -v <version>    Specify the version of PostgreSQL to install (must be an integer greater than 8)"
  echo "                  If not specified, the latest version will be installed."
  echo "  -y, --yes       Confirm the installation without prompting (must be preceded by '-')"
  echo "  -h, --help      Display this help message"
  echo ""
  echo "EXAMPLES:"
  echo "  $0                  Install the latest version of PostgreSQL with confirmation prompt"
  echo "  $0 -v 11 -y         Install PostgreSQL version 11 without confirmation prompt"
  echo "  $0 --version 10     Install PostgreSQL version 10 with confirmation prompt"
  echo ""
}

# Parse command-line arguments
while [[ "$#" -gt 0 ]]; do
  case $1 in
    -v|--version)
      if [[ "$2" =~ ^[0-9]+$ && "$2" -gt 8 ]]; then
        version=$2
        shift
      else
        echo "Invalid PostgreSQL version. Please specify an integer greater than 8."
        help
        exit 1
      fi
      ;;
    -y|--yes)
      if [[ "$2" =~ ^-y$ ]]; then
        confirm=true
        shift
      else
        echo "Invalid confirmation option. Please use '-y'."
        help
        exit 1
      fi
      ;;
    -h|--help)
      help
      exit
      ;;
    *)
      echo "Unknown option: $1"
      help
      exit 1
      ;;
  esac
  shift
done

# Set default values if options not defined
if [[ -z $version ]]; then
  version=$(apt-cache show postgresql | grep Version | head -n 1 | cut -d ' ' -f 2 | cut -d '-' -f 1)
fi
if [[ -z $confirm ]]; then
  read -p "Are you sure you want to install PostgreSQL version $version? (y/n) " choice
  case "$choice" in
    y|Y ) confirm=true;;
    * ) echo "Installation cancelled"; exit;;
  esac
fi

# Add PostgreSQL repository
echo "Adding PostgreSQL repository..."
sudo sh -c "echo \"deb http://apt.postgresql.org/pub/repos/apt \$(lsb_release -cs)-pgdg main\" > /etc/apt/sources.list.d/pgdg.list"
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

# Install PostgreSQL
echo "Installing PostgreSQL version $version..."
sudo apt-get update
sudo apt-get -y install postgresql-$version

# Check installation status
if [[ $? -eq 0 ]]; then
  echo "PostgreSQL version $version successfully installed!"
else
  echo "PostgreSQL installation failed."
fi
