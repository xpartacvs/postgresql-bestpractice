# PostgreSQL Best Practice in Ubuntu

[[_TOC_]]

## 1. Install PostgreSQL in Ubuntu

By assuming that you have already clone this repository, you can install PostgreSQL by running the following command:

```bash
# Add executable permission to pg_install file
chmod +x tools/bash/pg_install

# Run pg_install file
./tools/bash/pg_install
```

> :warning: **Warning**: The `pg_install` script will ask for your sudo password to install PostgreSQL. Make sure you have the permission to run the script.

### 1.1 (Optional) Install XFP for PostgreSQL user

You might want to install XFP to have a better prompt when you are working with BASH. By installing XFP, you will gain the following features:

- Git prompt
- Working directory prompt
- Some `ls` extra aliasses: `l`,`ll`,`la`,`lh`

To proceed, run the following commands:

```bash
# login as postgres user
sudo su - postgres

# Install XFP     
curl -sSL https://gitlab.com/xpartacvs/postgresql-bestpractice/-/raw/main/tools/bash/xfp/xfp-install | bash

# Reload bash
source ~/.bash_profile
```

> :bulb: **Tip**: XFP is also recomended for other users.

## 2. Hardening Host-based Authentication
